FROM ubuntu:18.04

LABEL name="robot-framework"
LABEL description="Robot Framework in Docker"
LABEL maintainer=lilian.calliot@miicom.fr

# Setup X Window Virtual Framebuffer
ENV SCREEN_COLOUR_DEPTH 24
ENV SCREEN_HEIGHT 1080
ENV SCREEN_WIDTH 1920

# Default set Robot Framework's threads
ENV ROBOT_THREADS 1

# Prepare scripts to be executed
COPY bin/run-tests-in-virtual-screen.sh /opt/robotframework/bin/run-tests-in-virtual-screen

# Install system dependencies
RUN apt-get update && apt-get install --no-install-recommends -y \
        curl \
        wget \
        python3 \
        python3-pip \
        python3-setuptools \
        xvfb \
        # Chrome packages
        chromium-browser \
        chromium-chromedriver \
        udev \
        # Firefox packages
        firefox \
        firefox-geckodriver \
        dbus \
        fonts-freefont-ttf \
        # Remove tmp and cache files
        && apt-get clean 
    # Install Robot Framework and Selenium Library (with pip)
RUN     pip3 install --upgrade pip \
        && pip3 install --upgrade robotframework \
        && pip3 install --upgrade selenium==4.0.0a1 \
        && pip3 install --upgrade robotframework-selenium2library \
        && pip3 install --upgrade robotframework-appiumlibrary \
        && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache/pip

# Update system path
ENV PATH=/opt/robotframework/bin:$PATH

# Robot Framework default user
RUN useradd -ms /bin/bash robot \
    && chmod -R 777 /etc/ssl/certs \
    && chmod -R +x /opt/robotframework/bin
USER robot
WORKDIR /home/robot

HEALTHCHECK NONE

CMD ["run-tests-in-virtual-screen"]